//
//  AppDelegate.h
//  WalkthroughOne
//
//  Created by Leon Gaffar on 23/05/2017.
//  Copyright © 2017 Leon Gaffar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

