//
//  main.m
//  WalkthroughOne
//
//  Created by Leon Gaffar on 23/05/2017.
//  Copyright © 2017 Leon Gaffar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
